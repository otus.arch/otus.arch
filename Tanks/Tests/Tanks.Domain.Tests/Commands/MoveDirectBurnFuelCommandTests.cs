﻿using Moq;
using NUnit.Framework;
using Tanks.Domain.Interfaces;

namespace Tanks.Domain.Commands.Tests
{
    public class MoveDirectBurnFuelCommandTests
    {
        private MoveDirectBurnFuelCommand Create(IUObject obj = null)
        {
            if (obj == null) obj = new Mock<IUObject>().Object;
            return new MoveDirectBurnFuelCommand(obj);
        }

        [Test]
        public void CTOR_CreatedObjectIsNotNullByDefault_True() => Assert.That(Create(), Is.Not.Null);

        [Test]
        public void Execute_AreEqualExpectedValues_True()
        {
            var mock = new Mock<IUObject>();
            mock.Setup(x => x["Position"]).Returns(new Vector(10, 5));
            mock.Setup(x => x["Velocity"]).Returns(new Vector(3, 2));
            mock.SetupSet(x => x["Position"] = new Vector(13,7)).Verifiable();
            
            
            mock.Setup(x => x["Fuel"]).Returns(100);
            mock.Setup(x => x["FuelForStep"]).Returns(10);
            mock.SetupSet(x => x["Fuel"] = 90).Verifiable();
            
            var command = Create(mock.Object);
            command.Execute();
            
            mock.Verify();
        }
    }
}