﻿using Moq;
using NUnit.Framework;
using Tanks.Domain.Interfaces;

namespace Tanks.Domain.Commands.Tests
{
    [TestFixture]
    public class BurnFuelCommandTests
    {
        private BurnFuelCommand Create(IFuelable fuelable = null)
        {
            if(fuelable == null)
            {
                var mock = new Mock<IFuelable>();
                mock.Setup(x => x.Fuel).Returns(100);
                mock.Setup(x => x.FuelForStep).Returns(10);
                fuelable = mock.Object;
            }
            return new BurnFuelCommand(fuelable);
        }

        [Test] public void CTOR_CreatedObjectIsNotNullByDefault_True() => Assert.That(Create(), Is.Not.Null);
        [TestCase(100, 10, 90)]
        [TestCase(100, -20, 120)]
        [TestCase(10, 100, -90)]
        public void Execute_FuelAreExpectedAfterBurn_True(int fuel, int fuelStep, int expected)
        {
            var mock = new Mock<IFuelable>();
            mock.Setup(x => x.Fuel).Returns(fuel);
            mock.Setup(x => x.FuelForStep).Returns(fuelStep);
            mock.SetupSet(x => x.Fuel = expected).Verifiable();

            var command = Create(mock.Object);
            command.Execute();
            mock.Verify();
        }
    }
}
