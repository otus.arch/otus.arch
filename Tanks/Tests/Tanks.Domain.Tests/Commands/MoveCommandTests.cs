﻿using Moq;
using NUnit.Framework;
using System;
using System.Linq;
using Tanks.Domain.Adapters;
using Tanks.Domain.Interfaces;
using Tanks.Domain.UObjects;

namespace Tanks.Domain.Commands.Tests
{
    [TestFixture]
    public class MoveCommandTests
    {
        private MoveCommand Create(IMovable movable = null)
        {
            if (movable == null)
            {
                var mock = new Mock<IMovable>();
                Vector position = new Vector(12, 5);
                Vector velocity = new Vector(-7, 3);
                mock.Setup(x => x.Position).Returns(position);
                mock.Setup(x => x.Velocity).Returns(velocity);
                movable = mock.Object;
            }
            return new MoveCommand(movable);
        }

        [Test] public void CTOR_CreatedObjectIsNotNullByDefault_True() => Assert.That(Create(), Is.Not.Null);

        [TestCase(12,5,-7,3,5,8)]
        [TestCase(-12,5,-7,3,-19,8)]
        [TestCase(12,5,-22,3,-10,8)]
        [TestCase(12,-5,-7,3,5,-2)]
        public void Move_NewPositionVectorAreEqualExpected_True(params int[] coordinates)
        {
            int range = coordinates.Length / 3;
            var position = new Vector(coordinates.Take(range).ToArray());
            var velocity = new Vector(coordinates.Skip(range).Take(range).ToArray());
            var expected = new Vector(coordinates.Skip(2*range).Take(range).ToArray());

            var movable = new Mock<IMovable>();
            movable.Setup(x => x.Position).Returns(position);
            movable.Setup(x => x.Velocity).Returns(velocity);
            movable.SetupSet(x => x.Position = expected).Verifiable();
            var move = Create(movable.Object);

            move.Execute();

            movable.Verify();
        }

        [TestCase(12, 5, -7, 3, 5, 8)]
        [TestCase(-12, 5, -7, 3, -19, 8)]
        [TestCase(12, 5, -22, 3, -10, 8)]
        [TestCase(12, -5, -7, 3, 5, -2)]
        public void Move_NotExistPositionVector_ArgumentException(params int[] coordinates)
        {
            int range = coordinates.Length / 3;
            var velocity = new Vector(coordinates.Skip(range).Take(range).ToArray());
             
            var movable = new Mock<IMovable>();
            movable.Setup(x => x.Velocity).Returns(velocity);
            var move = Create(movable.Object);
            Assert.Throws<ArgumentException>(() => move.Execute());
        }

        [TestCase(12, 5, -7, 3, 5, 8)]
        [TestCase(-12, 5, -7, 3, -19, 8)]
        [TestCase(12, 5, -22, 3, -10, 8)]
        [TestCase(12, -5, -7, 3, 5, -2)]
        public void Move_NotExistVelocityVector_ArgumentException(params int[] coordinates)
        {
            int range = coordinates.Length / 3;
            var position = new Vector(coordinates.Take(range).ToArray());

            var movable = new Mock<IMovable>();
            movable.Setup(x => x.Position).Returns(position);
            var move = Create(movable.Object);
            Assert.Throws<ArgumentException>(() => move.Execute());
        }
    }
}
