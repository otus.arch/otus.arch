﻿using Moq;
using NUnit.Framework;
using Tanks.Domain.Interfaces;

namespace Tanks.Domain.Commands.Tests
{
    public class ChangeVelocityCommandTests
    {
        private ChangeVelocityCommand Create(IChangeVelocity changeVelocity = null)
        {
            changeVelocity ??= new Mock<IChangeVelocity>().Object;
            return new ChangeVelocityCommand(changeVelocity);
        }

        [Test]
        public void CTOR_CreatedObjectIsNotNullByDefault_True() => Assert.That(Create(), Is.Not.Null);

        [Test]
        public void Execute_VelocityAreChanged_True()
        {
            var velocity = new Vector(2, 3);
            var expected = new Vector(0, 0 );

            var mock = new Mock<IChangeVelocity>();
            mock.Setup(x => x.Velocity).Returns(velocity);
            mock.SetupSet(x => x.Velocity = expected).Verifiable();

            var command = Create(mock.Object);
            command.Execute();
            
            mock.Verify();
        }
    }
}