﻿using Moq;
using NUnit.Framework;
using Tanks.Domain.Adapters;
using Tanks.Domain.Exceptions;
using Tanks.Domain.Interfaces;
using Tanks.Domain.UObjects;

namespace Tanks.Domain.Commands.Tests
{
    [TestFixture]
    public class MacroCommandTests
    {
        private MacroCommand Create(ITanksCommand[] commands = null)
        {
            commands ??= new ITanksCommand[]
            {
                new MoveCommand(new Mock<IMovable>().Object),
                new RotateCommand(new Mock<IRotable>().Object)
            };
            return new MacroCommand(commands);
        }

        [Test]
        public void CTOR_CreatedObjectIsNotNullByDefault_True() => Assert.That(Create(), Is.Not.Null);

        [Test]
        public void Execute_AreAllCommandExecuted_True()
        {
            var checkFieldCommand = new Mock<ITanksCommand>();
            checkFieldCommand.Setup(x => x.Execute()).Verifiable();

            var moveCommand = new Mock<ITanksCommand>();
            moveCommand.Setup(x => x.Execute()).Verifiable();

            var burnFuelCommand = new Mock<ITanksCommand>();
            burnFuelCommand.Setup(x => x.Execute()).Verifiable();

            MacroCommand macro = Create(new ITanksCommand[]
                {checkFieldCommand.Object, moveCommand.Object, burnFuelCommand.Object});
            macro.Execute();

            checkFieldCommand.Verify(x => x.Execute(), Times.Once);
            moveCommand.Verify(x => x.Execute(), Times.Once);
            burnFuelCommand.Verify(x => x.Execute(), Times.Once);
        }

        [Test]
        public void Execute_FueldNotEnough_CommandException()
        {
            Tank tank = new Tank();
            tank["Position"] = new Vector(12, 5);
            tank["Velocity"] = new Vector(-7, 5);
            tank["Fuel"] = 90;
            tank["FuelForStep"] = 100;

            var move = new MoveCommand(new MovableAdapter(tank));
            var fuelable = new FuelableAdapter(tank);
            var check = new CheckFuelCommand(fuelable);
            var burn = new BurnFuelCommand(fuelable);
            var macro = new MacroCommand(check, burn, move);

            Assert.Throws<CommandException>(() => macro.Execute());
        }

        [Test]
        public void Execute_CommandThrowException_StopExecutionAndThrowCommandException()
        {
            var command1 = new Mock<ITanksCommand>();
            command1.Setup(x => x.Execute()).Verifiable();
            var command2 = new Mock<ITanksCommand>();
            command2.Setup(x => x.Execute()).Throws(new CommandException());
            var command3 = new Mock<ITanksCommand>();
            command3.Setup(x => x.Execute()).Verifiable();
            var macro = new MacroCommand(command1.Object, command2.Object, command3.Object);

            Assert.Throws<CommandException>(() => macro.Execute());
            command1.Verify(x => x.Execute(), Times.Once);
            command2.Verify(x => x.Execute(), Times.Once);
            command3.Verify(x => x.Execute(), Times.Never);
        }

        [Test]
        public void Execute_MoveAndRotate_VelocityIsZero()
        {
            var velocity = new Vector(4, -3);
            var position = new Vector(10, 24);
            var expectedVelocity = new Vector(0, 0);

            var mock = new Mock<IUObject>();
            mock.Setup(x => x["Position"]).Returns(position);
            mock.Setup(x => x["Velocity"]).Returns(velocity);
            mock.SetupSet(x => x["Velocity"] = expectedVelocity).Verifiable();
            mock.Setup(x => x["Direction"]).Returns(3);
            mock.Setup(x => x["AngularVelocity"]).Returns(2);
            mock.Setup(x => x["MaxDirections"]).Returns(8);

            var command = Create(new ITanksCommand[]
            {
                new MoveCommand(new MovableAdapter(mock.Object)),
                new ChangeVelocityCommand(new ChangeVelocityAdapter(mock.Object)),
                new RotateCommand(new RotableAdapter(mock.Object))
            });

            command.Execute();
            mock.Verify();
        }

        [Test]
        public void RotateAndChangeVelocity_VelocityAreEqualZero_True()
        {
            var velocity = new Vector(4, -3);
            var position = new Vector(10, 24);
            var expectedVelocity = new Vector(0, 0);
            

                var mock = new Mock<IUObject>();
                mock.Setup(x => x["Position"]).Returns(position);
                mock.Setup(x => x["Velocity"]).Returns(velocity);
                mock.SetupSet(x => x["Velocity"] = expectedVelocity).Verifiable();
                
                mock.Setup(x => x["Direction"]).Returns(3);
                mock.Setup(x => x["AngularVelocity"]).Returns(2);
                mock.Setup(x => x["MaxDirections"]).Returns(8);
                
                Create(new ITanksCommand[]
                {
                    new ChangeVelocityCommand(new ChangeVelocityAdapter(mock.Object)),
                    new RotateCommand(new RotableAdapter(mock.Object))
                }).Execute();
                
                mock.Verify();
        }
        
        [Test]
        public void RotateAndChangeVelocity_VelocityNotExist_NoException()
        {
            var position = new Vector(10, 24);
            

            var mock = new Mock<IUObject>();
            mock.Setup(x => x["Position"]).Returns(position);
            mock.Setup(x => x["Direction"]).Returns(3);
            mock.Setup(x => x["AngularVelocity"]).Returns(2);
            mock.Setup(x => x["MaxDirections"]).Returns(8);
                
            var command = Create(new ITanksCommand[]
            {
                new ChangeVelocityCommand(new ChangeVelocityAdapter(mock.Object)),
                new RotateCommand(new RotableAdapter(mock.Object))
            });
            
            Assert.DoesNotThrow(command.Execute);
        }
    }
}
