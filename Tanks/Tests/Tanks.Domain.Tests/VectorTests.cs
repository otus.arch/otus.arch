using NUnit.Framework;
using System.Linq;

namespace Tanks.Domain.Tests
{
    public class VectorTests
    {
        private Vector Create(int[] body = null)
        {
            body ??= new int[] { 1, 2 };
            return new Vector(body);
        }

        [Test] public void CTOR_CreatedObjectIsNotNullByDefault_True() => Assert.That(Create(), Is.Not.Null);

        [TestCase(1, 2, 1, 1, 2, 3)]//x1,y1; x2,y2; x3,y3
        [TestCase(1, 2, 1, -1, 2, 1)]
        [TestCase(1, 2, -1, 1, 0, 3)]
        [TestCase(1, 1, 1, 2, 2, 2, 3, 3, 3)]//x1,y1,z1; x2,y2,z2; x3,y3,z3
        public void SumVectors_ResultAreExpected_True(params int[] coordinates)
        {
            int r = coordinates.Length / 3;
            Vector v1 = new(coordinates.Take(r).ToArray());
            Vector v2 = new(coordinates.Skip(r).Take(r).ToArray());
            Vector expected = new(coordinates.Skip(2 * r).Take(r).ToArray());

            Vector action = v1 + v2;

            Assert.That(action == expected, Is.True);
        }

        [TestCase(false, 1, 2, 1, 1)]//x1,y1; x2,y2; x3,y3
        [TestCase(true, 1, 2, 1, 2)]
        [TestCase(false, 1, 2, -1, 1, 0, 3)]
        [TestCase(true, 1, 2, -3, 1, 2, -3)]//x1,y1,z1; x2,y2,z2; x3,y3,z3
        public void Equals_ResultsAreExpected_True(bool expect, params int[] coordinates)
        {
            int r = coordinates.Length / 2;
            Vector v1 = new Vector(coordinates.Take(r).ToArray());
            Vector v2 = new Vector(coordinates.Skip(r).Take(r).ToArray());
            Assert.That(v1 == v2, Is.EqualTo(expect));
        }

        [TestCase(true, 1, 2, 1, 1)]//x1,y1; x2,y2; x3,y3
        [TestCase(false, 1, 2, 1, 2)]
        [TestCase(true, 1, 2, -1, 1, 0, 3)]
        [TestCase(false, 1, 2, -3, 1, 2, -3)]//x1,y1,z1; x2,y2,z2; x3,y3,z3
        public void NotEquals_ResultsAreExpected_True(bool expect, params int[] coordinates)
        {
            int r = coordinates.Length / 2;
            Vector v1 = new Vector(coordinates.Take(r).ToArray());
            Vector v2 = new Vector(coordinates.Skip(r).Take(r).ToArray());
            Assert.That(v1 != v2, Is.EqualTo(expect));
        }
    }
}