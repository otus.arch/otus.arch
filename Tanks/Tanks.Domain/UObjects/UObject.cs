﻿using System;
using System.Collections.Generic;
using Tanks.Domain.Interfaces;

namespace Tanks.Domain.UObjects
{
    public class UObject:IUObject
    {
        private readonly Dictionary<string, object> _dictionary = new Dictionary<string, object>();
        public object this[string property] 
        {
            get
            {
                if (_dictionary.ContainsKey(property)) return _dictionary[property];
                else throw new ArgumentException($"{nameof(UObject)} haven't property \'{property}\'", nameof(property));
            }
            set
            {
                if (_dictionary.ContainsKey(property)) _dictionary[property] = value;
                else _dictionary.Add(property, value);
            }
        }
    }
}