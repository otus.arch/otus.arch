﻿namespace Tanks.Domain.Interfaces
{
    public interface IMovable
    {
        Vector Position { get; set; }
        Vector Velocity { get; }
    }
}