﻿namespace Tanks.Domain.Interfaces
{
    public interface IUObject
    {
        object this[string property] { get;set; }
    }
}
