﻿namespace Tanks.Domain.Interfaces
{
    public interface IChangeVelocity
    {
        Vector Velocity { get; set; }
    }
}