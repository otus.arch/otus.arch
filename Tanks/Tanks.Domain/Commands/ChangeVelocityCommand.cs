﻿using Tanks.Domain.Interfaces;

namespace Tanks.Domain.Commands
{
    public class ChangeVelocityCommand : ITanksCommand
    {
        private readonly IChangeVelocity _changeble;
        public ChangeVelocityCommand(IChangeVelocity changeble)
        {
            this._changeble = changeble;
        }
        public void Execute()
        {
            _changeble.Velocity = new Vector(new[] {0, 0}); 
        }
    }
}