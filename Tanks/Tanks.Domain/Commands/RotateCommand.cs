﻿using System;
using Tanks.Domain.Interfaces;

namespace Tanks.Domain.Commands
{
    public class RotateCommand:ITanksCommand
    {
        public readonly IRotable rotable;
        public RotateCommand(IRotable rotable)
        {
            this.rotable = rotable;
        }
        public virtual void Execute()
        {
            rotable.Direction = Math.Abs((rotable.Direction + rotable.AngularVelocity) % rotable.MaxDirections);
        }
    }
}
