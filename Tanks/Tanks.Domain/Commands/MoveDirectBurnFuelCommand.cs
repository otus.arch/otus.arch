﻿using Tanks.Domain.Adapters;
using Tanks.Domain.Interfaces;

namespace Tanks.Domain.Commands
{
    public class MoveDirectBurnFuelCommand: MacroCommand
    {
        public MoveDirectBurnFuelCommand(IUObject obj)
        {
            var fuelable = new FuelableAdapter(obj);
            var movable = new MovableAdapter(obj);
            commands = new ITanksCommand[]
            {
                new CheckFuelCommand(fuelable), 
                new MoveCommand(movable),
                new BurnFuelCommand(fuelable)
            };
        }
    }
}