﻿using Tanks.Domain.Interfaces;

namespace Tanks.Domain.Commands
{
    public class NoMoveCommand : ITanksCommand
    {
        public NoMoveCommand(IMovable movable){}

        void ITanksCommand.Execute()
        {
            // This method is empty because class haven't implementation of this method; 
        } 
    }
}
