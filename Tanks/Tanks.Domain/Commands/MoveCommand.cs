﻿using System;
using Tanks.Domain.Interfaces;

namespace Tanks.Domain.Commands
{
    public class MoveCommand:ITanksCommand
    {
        private readonly IMovable movable;
        public MoveCommand(IMovable movable)
        {
            this.movable = movable;
        }

        public void Execute()
        {
            if (movable == null || movable.Position == null || movable.Velocity == null) throw new ArgumentException(nameof(movable));
            movable.Position += movable.Velocity;
        }
    }
}
