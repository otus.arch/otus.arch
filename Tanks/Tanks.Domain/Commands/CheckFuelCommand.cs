﻿using System;
using Tanks.Domain.Exceptions;
using Tanks.Domain.Interfaces;

namespace Tanks.Domain.Commands
{
    public class CheckFuelCommand : ITanksCommand
    {
        private readonly IFuelable checkableFuel;
        public CheckFuelCommand(IFuelable checkableFuel)
        {
            this.checkableFuel = checkableFuel;
        }
        public void Execute()
        {
            if ((checkableFuel.Fuel - checkableFuel.FuelForStep) < 0)
            {
                throw new CommandException("Fuel is not enough. You cannot move");
            }
        }
    }
}
