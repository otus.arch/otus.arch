﻿using Tanks.Domain.Interfaces;

namespace Tanks.Domain.Commands
{
    public class BurnFuelCommand : ITanksCommand
    {
        private readonly IFuelable fuelable;
        public BurnFuelCommand(IFuelable fuelable)
        {
            this.fuelable = fuelable;
        }
        public void Execute()
        {
            fuelable.Fuel -= fuelable.FuelForStep;
        }
    }
}
