﻿using Tanks.Domain.Interfaces;

namespace Tanks.Domain.Adapters
{
    public class MovableAdapter : IMovable
    {
        private readonly IUObject uObject;
        public MovableAdapter(IUObject uObject)
        {
            this.uObject = uObject;
        }
        public Vector Position { get => (Vector)uObject["Position"]; set => uObject["Position"] = value ; }
        public Vector Velocity => (Vector)uObject["Velocity"];
    }
}
