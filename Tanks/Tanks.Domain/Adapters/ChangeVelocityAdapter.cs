﻿using Tanks.Domain.Interfaces;

namespace Tanks.Domain.Adapters
{
    public class ChangeVelocityAdapter :IChangeVelocity
    {
        private readonly IUObject _uObject;
        public ChangeVelocityAdapter(IUObject uObject)
        {
            _uObject = uObject;
        }

        public Vector Velocity
        {
            get => (Vector)_uObject["Velocity"]; 
            set => _uObject["Velocity"] = value;
        }
    }
}