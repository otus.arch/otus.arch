﻿using Tanks.Domain.Interfaces;

namespace Tanks.Domain.Adapters
{
    public class RotableAdapter : IRotable
    {
        private readonly IUObject uObject;
        public RotableAdapter(IUObject uObject)
        {
            this.uObject = uObject;
        }
        public int Direction { get => (int)uObject["Direction"]; set => uObject["Direction"] = value; }
        public int AngularVelocity => (int)uObject["AngularVelocity"];
        public int MaxDirections => (int)uObject["MaxDirections"];
    }
}
