# Домашнее задание

Движение игровых объектов по полю.

## Цель:
- Выработка навыка применения SOLID принципов на примере игры "Танки".
- В результате выполнения ДЗ будет получен код, отвечающий за движение объектов по игровому полю, устойчивый к появлению новых игровых объектов и дополнительных ограничений, накладываемых на это движение.
- Реализовать движение объектов на игровом поле в рамках подсистемы Игровой сервер.

### Общие требования

- [x] ДЗ сдано на проверку - 2 балла
- [x] Код решения опубликован на github/gitlab - 1 балл
- [x] Настроен CI - 5 баллов
- [x] Код компилируется без ошибок - 1 балл.
- [x] Все тесты успешно выполняются - 1 балл
- [x] Прямолинейное равномерное движение без деформации.
- [x] [Само движение реализовано в виде отдельного класса](../Tanks/Tanks.Domain/Commands/MoveCommand.cs#L3) - 1 балл.
- [x] Для движущихся объектов [определен интерфейс](../Tanks/Tanks.Domain/Interfaces/IMovable.cs#L3), устойчивый к появлению новых видов движущихся объектов - 1 балл

### Тесты
- [x] [Реализован тест](../Tanks/Tests/Tanks.Domain.Tests/Commands/MoveCommandTests.cs#L29): Для объекта, находящегося в точке (12, 5) и движущегося со скоростью (-7, 3) движение меняет положение объекта на (5, 8) - 1 балл 
- [x] [Реализован тест](../Tanks/Tests/Tanks.Domain.Tests/Commands/MoveCommandTests.cs#L56): Попытка сдвинуть объект, у которого невозможно прочитать положение в пространстве, приводит к ошибке - 1 балл
- [x] [Реализован тест](../Tanks/Tests/Tanks.Domain.Tests/Commands/MoveCommandTests.cs#L73): Попытка сдвинуть объект, у которого невозможно прочитать значение мгновенной скорости, приводит к ошибке - 1балл
- [x] [Реализован тест](../Tanks/Tests/Tanks.Domain.Tests/Commands/MacroCommandTests.cs#L41): Попытка сдвинуть объект, у которого невозможно изменить положение в пространстве, приводит к ошибке - 1 балл
- [x] Поворот объекта вокруг оси.
- [x] [Сам поворот реализован в виде отдельного класса]((../Tanks/Tanks.Domain/Commands/RotateCommand.cs#L3)) - 1 балл
- [x] Для поворачивающегося объекта [определен интерфейс](../Tanks/Tanks.Domain/Interfaces/IRotable.cs#L3), устойчивый к появлению новых видов движущихся объектов - 1 балл
- [x] Реализован тесты - 4 балла (по одному баллу за тест, но не более 4-х баллов).
    - [Vector tests](../Tanks/Tests/Tanks.Domain.Tests/VectorTests.cs)
    - Commands
        - [x] [MoveCommandTests](../Tanks/Tests/Tanks.Domain.Tests/Commands/MoveCommandTests.cs)
        - [x] [RotateCommandTests](../Tanks/Tests/Tanks.Domain.Tests/Commands/RotateCommandTests.cs)
        - [x] [MacroCommandTests](../Tanks/Tests/Tanks.Domain.Tests/Commands/MacroCommandTests.cs)
        - [x] [CheckFuelCommandTests](../Tanks/Tests/Tanks.Domain.Tests/Commands/CheckFuelCommandTests.cs)
        - [x] [BurnFuelCommandTests](../Tanks/Tests/Tanks.Domain.Tests/Commands/BurnFuelCommandTests.cs)
    - UObjects
        - [x] [TankTests](../Tanks/Tests/Tanks.Domain.Tests/UObjects/TankTests.cs)