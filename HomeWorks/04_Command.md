# Макрокоманды

## Цель:

- Научиться обрабатывать ситуации с точки зрения SOLID, когда требуется уточнить существующее поведение без модификации существующего кода.

## Описание

Предположим, что у нас уже написаны следующие команды :

- ***MoveCommand*** ([see link](..\Tanks\Tanks.Domain\Commands\MoveCommand.cs))
- ***RotateCommand*** ([see link](..\Tanks\Tanks.Domain\Commands\RotateCommand.cs)). 

Теперь возникло новое требование: пользователи в игре могут устанавливать правило - во время движение расходуется топливо, двигаться можно только при наличии топлива.

- Реализовать новую возможность можно введя две новые команды:

    - **CheckFuelCommand** ([see link](..\Tanks\Tanks.Domain\Commands\CheckFuelCommand.cs)) - проверяет, что топлива достаточно, если нет, то выбрасывает исключение CommandException.

    - **BurnFuelCommand** ([see link](..\Tanks\Tanks.Domain\Commands\BurnFuelCommand.cs)) - уменьшает количество топлива на скорость расхода топлива.


- После этого мы можем три команды выстроить в цепочку.
**CheckFuelCommand** + **MoveCommand** + **BurnFuelCommand** ([see link](..\Tanks\Tests\Tanks.Domain.Tests\Commands\MacroCommandTests.cs#26))

- Чтобы это было прозрачно для пользователя реализуем Макрокоманду - специальную разновидность команды, которая в конструкторе  принимает массив команда, а методе execute их все последовательно выполняет. ([see link](..\Tanks\Tanks.Domain\Commands\MacroCommand.cs#8))

- При повороте движущегося объекта меняется вектор мгновенной скорости. Напишите команду, которая модифицирует вектор мгновенной скорости, в случае поворота. ([see link](..\Tanks\Tanks.Domain\Commands\ChangeVelocityCommand.cs))

- Постройте цепочку команд для поворота ([see link](..\Tanks\Tests\Tanks.Domain.Tests\Commands\MacroCommandTests.cs#81)).

## Тесты

- [x] Реализовать класс **CheckFuelComamnd** ([link](..\Tanks\Tanks.Domain\Commands\CheckFuelCommand.cs)) и **тесты к нему** ([link](..\Tanks\Tests\Tanks.Domain.Tests\Commands\CheckFuelCommandTests.cs)):
    - [x] проверяет, если не достаточно топлива, то выбрасывает исключение CommandException ([see link](..\Tanks\Tests\Tanks.Domain.Tests\Commands\CheckFuelCommandTests.cs#39))
    - [x] проверяет, что топлива достаточно, то не выбрасывает исключение ([see link](..\Tanks\Tests\Tanks.Domain.Tests\Commands\CheckFuelCommandTests.cs#27))
- [x] Реализовать класс <u>***BurnFuelCommand***</u> ([see link](..\Tanks\Tanks.Domain\Commands\BurnFuelCommand.cs)) и тесты к нему ([see link](..\Tanks\Tests\Tanks.Domain.Tests\Commands\BurnFuelCommandTests.cs)).
    - <u>**BurnFuelCommand**</u> уменьшает количество топлива на скорость расхода топлива ([see link](..\Tanks\Tests\Tanks.Domain.Tests\Commands\BurnFuelCommandTests.cs#26))
- [x] Реализовать простейшую макрокоманду ([see link](..\Tanks\Tanks.Domain\Commands\MacroCommand.cs)) и тесты к ней ([see link](..\Tanks\Tests\Tanks.Domain.Tests\Commands\MacroCommandTests.cs)). 

*Здесь простейшая - это значит, что при выбросе исключения вся последовательность команд приостанавливает свое выполнение, а макрокоманда выбрасывает **<u>CommandException**</u>.* ([see link](..\Tanks\Tests\Tanks.Domain.Tests\Commands\MacroCommandTests.cs#60))

- [x] Реализовать команду движения по прямой с расходом топлива, используя команды с предыдущих шагов. 
    - [link of implementation](..\Tanks\Tanks.Domain\Commands\MoveDirectBurnFuelCommand.cs)
    - [link of tests](..\Tanks\Tests\Tanks.Domain.Tests\Commands\MoveDirectBurnFuelCommandTests.cs)
- [x] Реализовать команду для модификации вектора мгновенной скорости при повороте. Необходимо учесть, что не каждый разворачивающийся объект движется. ([see link](..\Tanks\Tanks.Domain\Commands\ChangeVelocityCommand.cs))
- [-] Реализовать команду поворота, которая еще и меняет вектор мгновенной скорости, если есть. ([see link](..\Tanks\Tests\Tanks.Domain.Tests\Commands\MacroCommandTests.cs#109)

## Критерии оценки:
- Домашнее задание сдано - 2 балла.
- Реализована команда <u>***[CheckFuelCommand](..\Tanks\Tanks.Domain\Commands\CheckFuelCommand.cs)***</u> - 1балл
- Написаны тесты к <u>***[CheckFuelComamnd](..\Tanks\Tests\Tanks.Domain.Tests\Commands\CheckFuelCommandTests.cs)***</u> - 1 балл
- Реализована команда <u>***[BurnFuelCommand](..\Tanks\Tanks.Domain\Commands\BurnFuelCommand.cs)***</u> - 1балл
- Написаны тесты к <u>***[BurnFuelComamnd](..\Tanks\Tests\Tanks.Domain.Tests\Commands\BurnFuelCommandTests.cs)***</u> - 1 балл
- Реализована команда <u>***[MacroCommand](..\Tanks\Tanks.Domain\Commands\MacroCommand.cs)***</u> - 1балл
- <u>***[Реализована команда движения по прямой с расходом топлива](..\Tanks\Tanks.Domain\Commands\MoveDirectBurnFuelCommand.cs)***</u> и <u>***[тесты к ней](..\Tanks\Tests\Tanks.Domain.Tests\Commands\MoveDirectBurnFuelCommandTests.cs)***</u> - 1 балл
- Написаны тесты к <u>***[MacroComamnd](..\Tanks\Tests\Tanks.Domain.Tests\Commands\MacroCommandTests.cs)***</u> - 1 балл
- Реализована команда <u>***[ChangeVelocityCommand](..\Tanks\Tanks.Domain\Commands\ChangeVelocityCommand.cs)***</u> - 1балл
- Написаны тесты к <u>***[ChangeVelocityComamnd](..\Tanks\Tests\Tanks.Domain.Tests\Commands\ChangeVelocityCommandTests.cs)***</u> - 1 балл
- Реализована <u>***[команда поворота, которая еще и меняет вектор мгновенной скорости](..\Tanks\Tests\Tanks.Domain.Tests\Commands\MacroCommandTests.cs#109)***</u> - 1балл

**Итого: 12 баллов** 